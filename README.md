# ictlife-issue-tracking REST API

A RESTful API ictlife-issue-tracking with Go

It is just a RESTful API with Go using **gorilla/mux** (A nice mux library) and **gorm** (An ORM for Go)

## Installation & Run

```bash
# Download this project
go get github.com/Exuus/ictlife-issue-tracking
```

# configure db credentials appropriately in the config.json file

# To build for windows, 32 bit

gox -osarch="windows/386"

# Windows 64 bit build

gox -osarch="windows/amd64"

# All Builds

gox

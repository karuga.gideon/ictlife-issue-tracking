package main

import (
	"ictlife-issue-tracking/app"
	"ictlife-issue-tracking/config"
)

func main() {
	config := config.GetConfig()
	app := &app.App{}
	app.Initialize(config)
	app.Run(":9010")
}

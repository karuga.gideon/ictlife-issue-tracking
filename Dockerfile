# ictlife-issue-tracking docker file
FROM golang:1.10-alipine3.8
WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]
MAINTAINER karuga.gideon@gmail.com

# docker build -t ictlife-issue-tracking .
# docker run -it --rm --name my-running-app my-golang-app

# docker run -d -p 8089:8089 ictlife-issue-tracking


# Cross platform deployments 
# env GOOS=linux go build -o filename

package handler

import (
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/app/utils"
	"ictlife-issue-tracking/config"
	"log"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func CreateSysMsg(db *gorm.DB, newSysMsg model.SysMsg) {

	sysMsg := model.SysMsg{}
	var lastSysMsg model.SysMsg
	db.Last(&lastSysMsg)
	sysMsg.ID = lastSysMsg.ID + 1
	sysMsg.DateCreated = time.Now()
	sysMsg.DateSent = time.Now()

	sysMsg.MsgType = newSysMsg.MsgType
	sysMsg.Recipient = newSysMsg.Recipient
	sysMsg.Subject = newSysMsg.Subject
	sysMsg.Message = newSysMsg.Message
	sysMsg.Status = newSysMsg.Status
	sysMsg.IssueID = newSysMsg.IssueID

	db.Save(&sysMsg)
}

// GetAllSysMsgs - GetAllSysMsgs
func GetAllSysMsgs(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	sysMsgs := []model.SysMsg{}
	db.Order("Id DESC").Find(&sysMsgs)
	respondJSON(w, http.StatusOK, sysMsgs)
}

// GetSysMsg gets a user instance if exists, or respond the 404 error otherwise
func GetSysMsg(db *gorm.DB, sysMsgID int) *model.SysMsg {
	sysMsg := model.SysMsg{}
	if err := db.First(&sysMsg, model.SysMsg{ID: sysMsgID}).Error; err != nil {
		return &sysMsg
	}
	return &sysMsg
}

// SendMessages - SendMessages
func SendMessages(config config.Config, db *gorm.DB) {

	for {

		// Get all pending sys msgs - check status
		unsentMessages := []model.SysMsg{}
		db.Order("Id DESC").Where("status = 0 ").Limit(100).Find(&unsentMessages)

		for _, sysMsg := range unsentMessages {
			// Updated accordingly whether successful or failed
			response := model.SysMsg{}

			if sysMsg.MsgType == "EMAIL" {
				log.Println("About to Send Email ID >>> ", sysMsg.ID)
				response = utils.SendEmailViaLinodeServer(sysMsg)
				log.Println("Send Email Response    >>> ", response.Status)
			} else {
				log.Println("About to Send SMS ID >>> ", sysMsg.ID)
				response, _ = utils.SendSMS(sysMsg)
				log.Println("Send SMS Response    >>> ", response.Status)
			}

			if response.Status == 2 { // Success
				db.Model(&sysMsg).UpdateColumns(model.SysMsg{Status: 2, DateSent: time.Now()})
			} else if response.Status == 3 { // Fail
				db.Model(&sysMsg).UpdateColumns(model.SysMsg{Status: 3, DateSent: time.Now()})
			}

		}

		utils.DelaySeconds(1) // Check every 1 second
	}

}

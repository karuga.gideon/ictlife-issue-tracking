package handler

import (
	"encoding/json"
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/app/utils"
	"ictlife-issue-tracking/config"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// passphrase for passwords encryption
var passphrase = "USUCitKwDMONwYuZMRyEwOMuzXNSrGpuzvdvUrgW"

// Authenticate - authenticate user
func Authenticate(config *config.Config, db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	response := model.JSONResponse{}
	w.Header().Set("Content-Type", "application/json")
	basicAuth := r.Header.Get("Authorization")

	s := strings.Split(basicAuth, " ")
	value := s[1]
	details := utils.Base64DecodeString(value)

	userDetails := strings.Split(details, ":")
	email, password := userDetails[0], userDetails[1]

	validateUser := model.User{}
	_ = json.NewDecoder(r.Body).Decode(&validateUser)

	userValidation := model.User{}
	db.Where("email = ? and type = 1 ", email).Find(&userValidation).First(&userValidation)

	if userValidation.ID > 0 {

		activity := model.Activity{}
		activity.UserID = userValidation.ID
		activity.ActivityType = "USER_LOGIN"
		activity.Description = "Login Successful."
		activity.DateCreated = time.Now()

		decrypted, err := utils.DecryptString(userValidation.Password, passphrase)
		if err != nil {
			log.Println(err)
		}
		if password == decrypted {
			activity.Description = "Login Successful."
			utils.CreateTokenEndpointCustom(w, userValidation)
		} else {
			activity.Description = "Login Failed!"
			w.WriteHeader(http.StatusUnauthorized)
		}

		CreateActivity(db, activity)

	} else {
		response.Status = "01"
		response.Description = "Invalid Credentials!"
		w.WriteHeader(http.StatusUnauthorized)
	}

}

func GetAllUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	users := []model.User{}
	db.Order("Id DESC").Find(&users)
	respondJSON(w, http.StatusOK, users)
}

func FilterUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request, userType string) {
	customers := []model.User{}

	customersFilter := utils.PaginationFilterFromContext(r)
	sqlQuery := buildUserQueryFromFilter(customersFilter, userType)

	fullSQLQuery := "SELECT id, name, phone, email, type AS userType, password, date_created, updated_at FROM users " + sqlQuery

	rows, err := db.Raw(fullSQLQuery).Rows()

	if err != nil {
		log.Println("Encountered error while retrieving customers data >>> ", err.Error())
	}

	defer rows.Close()
	for rows.Next() {

		customer := model.User{}
		var id int
		var name string
		var phone string
		var email string
		var userType int
		var password string
		var dateCreated time.Time
		var updatedAt time.Time

		rows.Scan(&id, &name, &phone, &email, &userType, &password, &dateCreated, &updatedAt)

		customer.ID = id
		customer.Name = name
		customer.Phone = phone
		customer.Email = email
		customer.Type = userType
		customer.Password = password
		customer.DateCreated = dateCreated
		customer.DateUpdated = updatedAt

		customers = append(customers, customer)
	}

	respondJSON(w, http.StatusOK, customers)

}

func buildUserQueryFromFilter(reportsFilter model.ReportsFilter, userType string) string {

	query := "WHERE type = " + userType + " "

	if reportsFilter.StartDate != "" && reportsFilter.EndDate != "" {
		query += " AND DATE(date_created) between '" + reportsFilter.StartDate + "' and '" + reportsFilter.EndDate + "'"
	}

	if reportsFilter.Query != "" {
		query += " AND name LIKE '%" + reportsFilter.Query + "%' OR phone LIKE '%" + reportsFilter.Query + "%'  OR email LIKE '%" + reportsFilter.Query + "%' "
	}

	if reportsFilter.Page != 0 {

	}

	query += " ORDER BY name ASC "

	if reportsFilter.Per != 0 {
		query += " LIMIT " + utils.ConvertIntToString(reportsFilter.Per)
	}
	return query
}

// CreateUser - Creates a new user
func CreateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	user := model.User{}
	var lastUser model.User
	db.Last(&lastUser)
	user.ID = lastUser.ID + 1
	user.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	encrypted, err := utils.EncryptString(user.Password, passphrase)
	if err != nil {
		log.Println(err)
	} else {
		user.Password = encrypted
	}

	userExists := getUserByEmail(db, user.Email, w, r)
	if userExists != nil {
		if err := db.Save(&user).Error; err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}

		// Send Email
		// 1 - CX_Member, 2 - Customer
		message := "Dear " + user.Name + ", <br/><br/> You have been created on the Issue Desk System "
		if user.Type == 1 {
			message += " as a Customer Experience Admin."
		} else if user.Type == 2 {
			message += " as a Customer."
		}

		message += "<br/><br/>Kindly click or copy paste the link below to access the system "
		message += "<a href=\"http://176.58.124.136:3001/\">http://176.58.124.136:3001/</a><br/><br/>"
		message += "Kind Regards, <br/><br/>The Issue Desk Team"

		registrationEmail := model.SysMsg{}
		registrationEmail.MsgType = "EMAIL"
		registrationEmail.Recipient = user.Email
		registrationEmail.Subject = "Registration"
		registrationEmail.Message = message
		registrationEmail.Status = 0
		registrationEmail.IssueID = 0
		registrationEmail.DateCreated = time.Now()
		registrationEmail.DateSent = time.Now()
		CreateSysMsg(db, registrationEmail)

		activity := model.Activity{}
		activity.UserID = user.ID
		activity.ActivityType = "USER_CREATION"
		activity.Description = "User created successfully."
		activity.DateCreated = time.Now()
		CreateActivity(db, activity)

		respondJSON(w, http.StatusCreated, user)
	}

}

func GetUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func UpdateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	var user = getUserOr404(db, vars["id"], w, r)
	prevDate := user.DateCreated
	password, err := utils.EncryptString(user.Password, passphrase)
	if err != nil {
		log.Println(err)
	} else {
		user.Password = password
	}
	user.DateCreated = prevDate
	user.DateUpdated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, user)
}

// func UpdateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

// 	vars := mux.Vars(r)
// 	user_id := vars["id"]
// 	user := getUserOr404(db, user_id, w, r)
// 	if user == nil {
// 		return
// 	}

// 	updateUserDetails := model.User{}
// 	decoder := json.NewDecoder(r.Body)
// 	if err := decoder.Decode(&updateUserDetails); err != nil {
// 		respondError(w, http.StatusBadRequest, err.Error())
// 		return
// 	}
// 	defer r.Body.Close()

// 	if updateUserDetails.Password != "" {
// 		// Encrypt the users pwd before saving to db
// 		encrypted, err := utils.EncryptString(updateUserDetails.Password, passphrase)
// 		if err != nil {
// 			log.Println(err)
// 		} else {
// 			user.Password = encrypted
// 		}
// 	}

// 	if err := db.Save(&user).Error; err != nil {
// 		respondError(w, http.StatusInternalServerError, err.Error())
// 		return
// 	}
// 	respondJSON(w, http.StatusOK, user)
// }

func DeleteUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Delete(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusNoContent, nil)
}

func ArchiveUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

func RestoreUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user_id := vars["id"]
	user := getUserOr404(db, user_id, w, r)
	if user == nil {
		return
	}
	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK, user)
}

// GeneratePassword
func GeneratePassword(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	pwd := utils.GenerateRandomString(40)
	respondJSON(w, http.StatusOK, pwd)
}

func CountUsers(db *gorm.DB, reportsFilter model.ReportsFilter) int64 {

	var count int64

	// if reportsFilter.StartDate.IsZero() && reportsFilter.EndDate.IsZero() {
	if reportsFilter.StartDate != "" && reportsFilter.EndDate != "" {
		db.Table("users").Count(&count)
	} else {
		db.Table("users").Where("date_created BETWEEN ? and ? ",
			reportsFilter.StartDate, reportsFilter.EndDate).Count(&count)
	}

	return count
}

// CountUsersByType - 1 - CX_Member, 2 - Customer
func CountUsersByType(db *gorm.DB, typeID int) int64 {
	var count int64
	if typeID == 100 {
		db.Table("users").Count(&count)
	} else {
		db.Table("users").Where("type = ? ", typeID).Count(&count)
	}
	return count
}

func CountUsersByMonthAndType(db *gorm.DB, month, typeID int) int64 {
	var count int64
	db.Table("users").Where("MONTH(date_created) = ? AND YEAR(date_created) = YEAR(CURRENT_DATE()) AND type = ? ", month, typeID).Count(&count)
	return count
}

// GetUserDetails - GetUserDetails
func GetUserDetails(db *gorm.DB, userID int) *model.User {
	user := model.User{}
	if err := db.First(&user, model.User{ID: userID}).Error; err != nil {
		return &user
	}
	return &user
}

// getUserOr404 gets a user instance if exists, or respond the 404 error otherwise
func getUserOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.User {
	user := model.User{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.User{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}

func getUserByEmail(db *gorm.DB, email string, w http.ResponseWriter, r *http.Request) *model.User {
	user := model.User{}

	if err := db.First(&user, model.User{Email: email}).Error; err != nil {
		return &user
	}
	respondError(w, http.StatusFound, "user already exists")
	return nil
}

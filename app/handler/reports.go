package handler

import (
	"ictlife-issue-tracking/app/model"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

// GetDashboardReport - returns data to be presented on the front-end dashboard
func GetDashboardReport(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	dashboardReport := model.DashboardReport{}

	issuesReport := model.IssuesReport{}

	// 0 - Open, 1 - Ongoing, 2 - Resolved, 3 - Follow up required.
	issuesReport.TotalOpen = CountIssuesByStatus(db, 0)
	issuesReport.TotalOngoing = CountIssuesByStatus(db, 1)
	issuesReport.TotalResolved = CountIssuesByStatus(db, 2)
	issuesReport.TotalFollowUpRequired = CountIssuesByStatus(db, 3)
	issuesReport.GrandTotal = CountIssuesByStatus(db, 100)

	monthlyIssuesReport := model.MonthlyIssuesReport{}
	currentMonth := int(time.Now().Month())
	monthlyIssuesReport.Month = currentMonth
	currentMonthIssuesReport := model.IssuesReport{}
	currentMonthIssuesReport.TotalOpen = CountIssuesByMonthAndStatus(db, currentMonth, 0)
	currentMonthIssuesReport.TotalOngoing = CountIssuesByMonthAndStatus(db, currentMonth, 1)
	currentMonthIssuesReport.TotalResolved = CountIssuesByMonthAndStatus(db, currentMonth, 2)
	currentMonthIssuesReport.TotalFollowUpRequired = CountIssuesByMonthAndStatus(db, currentMonth, 3)
	currentMonthIssuesReport.GrandTotal = CountIssuesByMonthAndStatus(db, currentMonth, 100)
	monthlyIssuesReport.IssuesReport = currentMonthIssuesReport

	yearlyIssuesReport := []model.MonthlyIssuesReport{}
	months := [12]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}

	for _, month := range months {
		monthIssuesReport := model.MonthlyIssuesReport{}
		monthIssuesReport.Month = month

		monthlyhIssuesReport := model.IssuesReport{}
		monthlyhIssuesReport.TotalOpen = CountIssuesByMonthAndStatus(db, month, 0)
		monthlyhIssuesReport.TotalOngoing = CountIssuesByMonthAndStatus(db, month, 1)
		monthlyhIssuesReport.TotalResolved = CountIssuesByMonthAndStatus(db, month, 2)
		monthlyhIssuesReport.TotalFollowUpRequired = CountIssuesByMonthAndStatus(db, month, 3)
		monthlyhIssuesReport.GrandTotal = CountIssuesByMonthAndStatus(db, month, 100)

		monthIssuesReport.IssuesReport = monthlyhIssuesReport
		yearlyIssuesReport = append(yearlyIssuesReport, monthIssuesReport)
	}

	dashboardReport.IssuesReport = issuesReport
	dashboardReport.MonthlyIssuesReport = monthlyIssuesReport
	dashboardReport.YearlyIssuesReport = yearlyIssuesReport

	respondJSON(w, http.StatusOK, dashboardReport)
}

// GetCustomersReport - View registered customers count - with a break down by month
func GetCustomersReport(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	customersReport := model.CustomersReport{}

	customersReport.AllCustomersCount = CountUsersByType(db, 2)

	allMonthsCustomersReport := []model.MonthlyCustomersReport{}
	months := [12]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}

	for _, month := range months {
		monthlyReport := model.MonthlyCustomersReport{}
		monthlyReport.Month = month
		monthlyReport.CustomersCount = CountUsersByMonthAndType(db, month, 2)
		allMonthsCustomersReport = append(allMonthsCustomersReport, monthlyReport)
	}

	customersReport.MonthlyCustomersReport = allMonthsCustomersReport

	respondJSON(w, http.StatusOK, customersReport)

}

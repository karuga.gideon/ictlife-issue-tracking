package handler

import (
	"encoding/json"
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/app/utils"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func CreateResolution(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	resolution := model.Resolution{}
	var lastResolution model.Resolution
	db.Last(&lastResolution)
	resolution.ID = lastResolution.ID + 1
	resolution.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&resolution); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&resolution).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if resolution.StatusID == 2 {

		var issue = model.Issue{ID: resolution.IssueID}
		resolutionAction := issue.Action + " Resolution: " + resolution.Resolution
		db.Model(&issue).Update("status_id", 2)
		db.Model(&issue).Update("action", resolutionAction)

		// Send emails to show that issue has been resolved
		customerNotification := model.SysMsg{}
		customerNotification.MsgType = "EMAIL"
		customerNotification.Recipient = "steve.wanjohi@ictlife.com"
		customerNotification.Subject = "ISSUE RESOLVED"
		customerNotification.Message = "Dear Customer, <br/><br/>Your issue [" + utils.ConvertIntToString(issue.ID) + "] has been resolved.<br/><br/>Issue details: " + issue.IssueDetails
		customerNotification.Status = 0
		customerNotification.IssueID = issue.ID
		customerNotification.DateCreated = time.Now()
		customerNotification.DateSent = time.Now()
		CreateSysMsg(db, customerNotification)

		SendIssueEmails(db, issue, "ISSUE RESOLVED", "The following issue ["+utils.ConvertIntToString(issue.ID)+"] has been marked as resolved: <br/><br/>"+issue.IssueDetails)
	}

	activity := model.Activity{}
	activity.UserID = resolution.ID
	activity.ActivityType = "RESOLUTION_CREATION"
	activity.Description = "New issue resolution created successfully."
	activity.DateCreated = time.Now()
	CreateActivity(db, activity)

	respondJSON(w, http.StatusCreated, resolution)

}

func UpdateResolution(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	var resolution = getResolutionOr404(db, vars["id"], w, r)
	prevDate := resolution.DateCreated
	resolution.DateCreated = prevDate

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&resolution); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&resolution).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, resolution)
}

func GetIssueResolutions(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	resolutions := []model.Resolution{}
	vars := mux.Vars(r)
	issue_id, err := strconv.Atoi(vars["issue_id"])

	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	db.Order("Id DESC").Find(&resolutions, "issue_id = ?", issue_id)
	respondJSON(w, http.StatusOK, resolutions)
}

// func GetResolution(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	issue_id := vars["id"]
// 	issue := getResolutionOr404(db, issue_id, w, r)
// 	if issue == nil {
// 		return
// 	}
// 	respondJSON(w, http.StatusOK, issue)
// }

func getResolutionOr404(db *gorm.DB, resolution_id string, w http.ResponseWriter, r *http.Request) *model.Resolution {
	resolution := model.Resolution{}
	n, err := strconv.Atoi(resolution_id)
	if err != nil {
		log.Println("Error converting resolution_id to int >>> ", err)
	}

	if err := db.First(&resolution, model.Resolution{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &resolution
}

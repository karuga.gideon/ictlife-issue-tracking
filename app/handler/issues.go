package handler

import (
	"encoding/json"
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/app/utils"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func CreateIssue(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	issue := model.Issue{}
	var lastIssue model.Issue
	db.Last(&lastIssue)
	issue.ID = lastIssue.ID + 1
	issue.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&issue); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&issue).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	activity := model.Activity{}
	activity.UserID = issue.ID
	activity.ActivityType = "ISSUE_CREATION"
	activity.Description = "New issue created successfully."
	activity.DateCreated = time.Now()
	CreateActivity(db, activity)

	customerNotification := model.SysMsg{}
	customerNotification.MsgType = "EMAIL"
	customerNotification.Recipient = "steve.wanjohi@ictlife.com"
	customerNotification.Subject = "ISSUE RECEIPT"

	message := "Dear Customer, <br/></br>Thank you for contacting us.<br/>"
	message += "Your issue has been noted and is being worked on. We will get back to you once it has been resolved.<br/>"
	message += "<br/>Kind Regards, <br/><br/>The Issue Desk Team"

	customerNotification.Message = message
	customerNotification.Status = 0
	customerNotification.IssueID = issue.ID
	customerNotification.DateCreated = time.Now()
	customerNotification.DateSent = time.Now()
	CreateSysMsg(db, customerNotification)

	rawMessage := "New Issue Created [" + utils.ConvertIntToString(issue.ID) + "], <br/><br/>"
	rawMessage += "<b>Customer Phone / Email :</b> " + issue.CustomerEmail + "<br/>"
	rawMessage += "<b>Issue Details :</b> " + issue.IssueDetails
	SendIssueEmails(db, issue, "NEW ISSUE", rawMessage)

	respondJSON(w, http.StatusCreated, issue)

}

func UpdateIssue(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	// issue_id, err := strconv.Atoi(vars["id"])
	// if err != nil {
	// 	log.Println("Error converting issue_id to int >>> ", err)
	// }
	// var issue = model.Issue{ID: issue_id}
	var issue = getIssueOr404(db, vars["id"], w, r)
	prevDate := issue.DateCreated
	issue.DateCreated = prevDate
	issue.DateUpdated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&issue); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&issue).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, issue)
}

func ResolveIssue(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	issue_id, err := strconv.Atoi(vars["id"])
	if err != nil {
		log.Println("Error converting issue_id to int >>> ", err)
	}
	var issue = model.Issue{ID: issue_id}
	db.Model(&issue).Update("status_id", 2)

	message := "Dear Customer, <br/><br/>"
	message += "Your issue [" + utils.ConvertIntToString(issue.ID) + "] has been resolved.<br/><br/>"
	message += "Issue details: " + issue.IssueDetails + "<br/><br/>"
	message += "Thank you for your business."
	message += "<br/>Kind Regards, <br/><br/>The Issue Desk Team"

	// Send emails to show that issue has been resolved
	customerNotification := model.SysMsg{}
	customerNotification.MsgType = "EMAIL"
	customerNotification.Recipient = "steve.wanjohi@ictlife.com"
	customerNotification.Subject = "ISSUE RESOLVED"
	customerNotification.Message = message
	customerNotification.Status = 0
	customerNotification.IssueID = issue.ID
	customerNotification.DateCreated = time.Now()
	customerNotification.DateSent = time.Now()
	CreateSysMsg(db, customerNotification)

	rawMessage := "The following issue [" + utils.ConvertIntToString(issue.ID) + "] "
	rawMessage += "has been marked as resolved: <br/><br/><b>Issue Details</b><br/><br/>" + issue.IssueDetails
	SendIssueEmails(db, issue, "ISSUE RESOLVED", rawMessage)
	respondJSON(w, http.StatusOK, issue)
}

func SendIssueEmails(db *gorm.DB, issue model.Issue, subject, rawMessage string) {

	recipientEmails := []string{"lucy@focusmobile.co", "karuga.gideon@gmail.com", "gidemn@gmail.com"}

	message := "Dear Customer Experience Team, <br/><br/>"
	message += rawMessage + "<br/><br/>"
	message += "Kindly click or copy paste the link below to access the system "
	message += "<a href=\"http://176.58.124.136:3001/\">http://176.58.124.136:3001/</a><br/><br/>"
	message += "<br/>Kind Regards, <br/><br/>The Issue Desk Team"

	for _, email := range recipientEmails {
		cxNotification := model.SysMsg{}
		cxNotification.MsgType = "EMAIL"
		cxNotification.Recipient = email
		cxNotification.Subject = subject
		cxNotification.Message = message
		cxNotification.Status = 0
		cxNotification.IssueID = issue.ID
		cxNotification.DateCreated = time.Now()
		cxNotification.DateSent = time.Now()
		CreateSysMsg(db, cxNotification)
	}

}

func GetAllIssues(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	issues := []model.Issue{}
	db.Order("Id DESC").Find(&issues)
	respondJSON(w, http.StatusOK, issues)
}

// FilterIssues - by Date, Status, and Query
func FilterIssues(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	issues := []model.Issue{}

	reportsFilter := utils.PaginationFilterFromContext(r)
	sqlQuery := buildQueryFromFilter(reportsFilter)

	// Filter by : date_created, status_id, issue_details
	// StatusID  : 0 - Open, 1 - Ongoing, 2 - Resolved / Closed, 3 - Follow up required.

	fullSQLQuery := "SELECT id, date_created, date_updated, customer_id, customer_email, channel_id, query_issue, " +
		"issue_details, assigned_to, status_id, action, created_by, updated_at FROM issues " + sqlQuery
	// log.Println("fullSQLQuery  >>> ", fullSQLQuery)
	rows, err := db.Raw(fullSQLQuery).Rows()

	if err != nil {
		log.Println("Encountered error while retrieving issues data >>> ", err.Error())
	}

	defer rows.Close()
	for rows.Next() {

		issue := model.Issue{}

		var id int
		var dateCreated time.Time
		var dateUpdated time.Time
		var customerID int
		var customerEmail string
		var channelID int
		var queryIssue string
		var issueDetails string
		var assignedTo int
		var statusID int
		var action string
		var createdBy string
		var updatedAt time.Time

		err = rows.Scan(&id, &dateCreated, &dateUpdated, &customerID, &customerEmail, &channelID, &queryIssue, &issueDetails,
			&assignedTo, &statusID, &action, &createdBy, &updatedAt)

		if err != nil {
			log.Println("Error scanning value into struct >>> ", err.Error())
		}

		issue.ID = id
		issue.DateCreated = dateCreated
		issue.DateUpdated = dateUpdated
		issue.CustomerID = customerID
		issue.CustomerEmail = customerEmail
		issue.ChannelID = channelID
		issue.QueryIssue = queryIssue
		issue.IssueDetails = issueDetails
		issue.AssignedTo = assignedTo
		issue.StatusID = statusID
		issue.Action = action
		issue.CreatedBy = createdBy
		issue.UpdatedAt = updatedAt

		issues = append(issues, issue)
	}

	respondJSON(w, http.StatusOK, issues)
}

func buildQueryFromFilter(reportsFilter model.ReportsFilter) string {

	query := "WHERE 1=1 "

	if reportsFilter.StartDate != "" && reportsFilter.EndDate != "" {
		query += " AND date_created between '" + reportsFilter.StartDate + "' and '" + reportsFilter.EndDate + "'"
	}

	if reportsFilter.Status != "" {
		query += " AND status_id = " + reportsFilter.Status
	}

	if reportsFilter.Query != "" {
		query += " AND issue_details like '%" + reportsFilter.Query + "%' or customer_email like '%" + reportsFilter.Query + "%' "
	}

	if reportsFilter.Page != 0 {

	}

	if reportsFilter.Per != 0 {
		query += " LIMIT %d" + utils.ConvertIntToString(reportsFilter.Per)
	}

	query += " ORDER BY id DESC "
	return query
}

func CountIssuesByStatus(db *gorm.DB, statusID int) int64 {
	var count int64
	if statusID == 100 {
		db.Table("issues").Count(&count)
	} else {
		db.Table("issues").Where("status_id = ? ", statusID).Count(&count)
	}
	return count
}

func CountIssuesByMonthAndStatus(db *gorm.DB, month, statusID int) int64 {
	var count int64
	if statusID == 100 {
		db.Table("issues").Where("MONTH(date_created) = ? AND YEAR(date_created) = YEAR(CURRENT_DATE())", month).Count(&count)
	} else {
		db.Table("issues").Where("MONTH(date_created) = ? AND YEAR(date_created) = YEAR(CURRENT_DATE()) AND status_id = ? ", month, statusID).Count(&count)
	}
	return count
}

func GetIssueDetails(db *gorm.DB, issueID int) *model.Issue {
	issue := model.Issue{}
	if err := db.First(&issue, model.Issue{ID: issueID}).Error; err != nil {
		return &issue
	}
	return &issue
}

func GetIssue(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	issue_id := vars["id"]
	issue := getIssueOr404(db, issue_id, w, r)
	if issue == nil {
		return
	}
	respondJSON(w, http.StatusOK, issue)
}

func getIssueOr404(db *gorm.DB, issue_id string, w http.ResponseWriter, r *http.Request) *model.Issue {
	issue := model.Issue{}
	n, err := strconv.Atoi(issue_id)
	if err != nil {
		log.Println("Error converting issue_id to int >>> ", err)
	}

	if err := db.First(&issue, model.Issue{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &issue
}

package handler

import (
	"ictlife-issue-tracking/app/model"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func CreateActivity(db *gorm.DB, newActivity model.Activity) {
	activity := model.Activity{}
	var lastActivity model.Activity
	db.Last(&lastActivity)
	activity.ID = lastActivity.ID + 1
	activity.DateCreated = time.Now()
	activity.UserID = newActivity.UserID
	activity.ActivityType = newActivity.ActivityType
	activity.Description = newActivity.Description
	db.Save(&activity)
}

func GetAllActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	activity := []model.Activity{}
	db.Order("Id DESC").Find(&activity)
	respondJSON(w, http.StatusOK, activity)
}

func GetActivity(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	activityID := vars["id"]
	user := getActivityOr404(db, activityID, w, r)
	if user == nil {
		return
	}
	respondJSON(w, http.StatusOK, user)
}

// getActivityOr404 gets a user instance if exists, or respond the 404 error otherwise
func getActivityOr404(db *gorm.DB, user_id string, w http.ResponseWriter, r *http.Request) *model.Activity {
	user := model.Activity{}
	n, err := strconv.Atoi(user_id)
	if err != nil {
		log.Println("Error converting user_id to int >>> ", err)
	}

	if err := db.First(&user, model.Activity{ID: n}).Error; err != nil {
		respondError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return &user
}

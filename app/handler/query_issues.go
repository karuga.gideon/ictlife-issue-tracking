package handler

import (
	"encoding/json"
	"ictlife-issue-tracking/app/model"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func CreateQueryIssues(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	qissue := model.QueryIssue{}
	var lastQueryIssue model.QueryIssue
	db.Last(&lastQueryIssue)
	qissue.ID = lastQueryIssue.ID + 1
	qissue.DateCreated = time.Now()

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&qissue); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if err := db.Save(&qissue).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	activity := model.Activity{}
	activity.UserID = qissue.CreatedBy
	activity.ActivityType = "QUERY_ISSUE_CREATION"
	activity.Description = "New issue type created successfully."
	activity.DateCreated = time.Now()
	CreateActivity(db, activity)

	respondJSON(w, http.StatusCreated, qissue)

}

func GetQueryIssues(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	qissues := []model.QueryIssue{}
	db.Order("issue_type ASC").Find(&qissues)
	respondJSON(w, http.StatusOK, qissues)
}

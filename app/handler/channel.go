package handler

import (
	"encoding/json"
	"ictlife-issue-tracking/app/model"
	"net/http"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

func CreateChannel(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	commchannel := model.CommChannel{}
	var lastCommChannel model.CommChannel
	db.Last(&lastCommChannel)
	commchannel.ID = lastCommChannel.ID + 1
	commchannel.DateCreated = time.Now()
	channeltype := strings.TrimSpace(commchannel.ChannelType)
	commchannel.ChannelType = channeltype

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&commchannel); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	channelExists := checkIfChannelExists(db, commchannel.ChannelType, w, r)
	if channelExists != nil {
		if err := db.Save(&commchannel).Error; err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}

		activity := model.Activity{}
		activity.UserID = commchannel.CreatedBy
		activity.ActivityType = "COMMUNICATION_CHANNEL_CREATION"
		activity.Description = "New communication channel created successfully."
		activity.DateCreated = time.Now()
		CreateActivity(db, activity)

		respondJSON(w, http.StatusCreated, commchannel)
	}

}

func GetChannels(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	commchannels := []model.CommChannel{}
	db.Order("channel_type ASC").Find(&commchannels)
	respondJSON(w, http.StatusOK, commchannels)
}

func checkIfChannelExists(db *gorm.DB, channel_type string, w http.ResponseWriter, r *http.Request) *model.CommChannel {
	channel := model.CommChannel{}

	if err := db.First(&channel, model.CommChannel{ChannelType: channel_type}).Error; err != nil {
		return &channel
	}
	respondError(w, http.StatusFound, "channel already exists")
	return nil
}

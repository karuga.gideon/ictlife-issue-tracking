package handler

import (
	"ictlife-issue-tracking/app/model"
	"net/http"

	"github.com/jinzhu/gorm"
)

// GetAllCXMembers - GetAllCXMembers
func GetAllCXMembers(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	users := []model.User{}
	db.Order("Id DESC").Find(&users)
	respondJSON(w, http.StatusOK, users)
}

// GetCXMemberDetails - GetCXMemberDetails
func GetCXMemberDetails(db *gorm.DB, id int) *model.User {
	cxMember := model.User{}
	db.Where("id = ? ", id).Find(&cxMember).First(&cxMember)
	return &cxMember
}

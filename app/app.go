package app

import (
	"log"
	"net/http"
	"strconv"

	"ictlife-issue-tracking/app/handler"
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/app/utils"
	"ictlife-issue-tracking/config"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	// MySql driver import
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
	Config *config.Config
}

// Initialize initializes the app with predefined configuration
func (a *App) Initialize(config config.Config) {

	mysqlConnection := config.Database.Username + ":" + config.Database.Password + "@/" + config.Database.Name + "?charset=utf8&parseTime=True&loc=Local"
	log.Println("MySql Db Connection String >>>  : ", mysqlConnection)
	db, err := gorm.Open("mysql", mysqlConnection)
	utils.CheckError(err, "MySql DB Connection")

	a.DB = model.DBMigrate(db)
	a.Router = mux.NewRouter()

	a.Config = &config

	downloadsDir := "/downloads/"
	a.Router.
		PathPrefix(downloadsDir).
		Handler(http.StripPrefix(downloadsDir, http.FileServer(http.Dir("."+downloadsDir))))

	a.setRouters()

	go handler.SendMessages(config, db)
}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// Options wraps the router for OPTIONS method
func (a *App) Options(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("OPTIONS")
}

// setRouters sets the all required routers
func (a *App) setRouters() {

	a.Get("/issue_tracking/authenticate", a.Authenticate)

	a.Post("/issue_tracking/users", a.CreateUser)
	a.Get("/issue_tracking/users/{limit}/{offset}/{search}", utils.ValidateMiddleware(a.GetAllUsers))
	a.Get("/issue_tracking/users", utils.ValidateMiddleware(a.FilterUsers))
	a.Get("/issue_tracking/customers", utils.ValidateMiddleware(a.FilterCustomers))
	a.Get("/issue_tracking/users/{id}", utils.ValidateMiddleware(a.GetUser))
	a.Post("/issue_tracking/users/{id}", utils.ValidateMiddleware(a.UpdateUser))

	a.Post("/issue_tracking/issues", a.CreateIssue)
	a.Get("/issue_tracking/issues", a.FilterIssues)
	a.Get("/issue_tracking/issues/{id}", utils.ValidateMiddleware(a.GetIssue))
	a.Post("/issue_tracking/issues/{id}", utils.ValidateMiddleware(a.UpdateIssue))
	a.Get("/issue_tracking/issues/resolve/{id}", utils.ValidateMiddleware(a.ResolveIssue))

	a.Post("/issue_tracking/resolutions", utils.ValidateMiddleware(a.CreateResolution))
	a.Post("/issue_tracking/resolutions/{id}", utils.ValidateMiddleware(a.UpdateResolution))
	a.Get("/issue_tracking/resolutions/{issue_id}", utils.ValidateMiddleware(a.GetIssueResolutions))

	a.Get("/issue_tracking/dashboard", utils.ValidateMiddleware(a.GetDashboardReport))

	a.Get("/issue_tracking/reports/customers", utils.ValidateMiddleware(a.GetCustomersReport))

	a.Post("/issue_tracking/query_issues", a.CreateQueryIssues)
	a.Get("/issue_tracking/query_issues", a.GetQueryIssues)

	a.Post("/issue_tracking/channels", a.CreateChannel)
	a.Get("/issue_tracking/channels", a.GetChannels)

	// a.Put(appURLName+"users/{id}", utils.ValidateMiddleware(a.UpdateUser))
	// a.Post(appURLName+"users/reset_pwd/{id}", utils.ValidateMiddleware(a.ResetUserPwd))
	// a.Get(appURLName+"users/generate_pwd", utils.ValidateMiddleware(a.GeneratePassword))

}

func (a *App) Authenticate(w http.ResponseWriter, r *http.Request) {
	handler.Authenticate(a.Config, a.DB, w, r)
}

func (a *App) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	handler.GetAllUsers(a.DB, w, r)
}

func (a *App) FilterUsers(w http.ResponseWriter, r *http.Request) {
	handler.FilterUsers(a.DB, w, r, strconv.Itoa(1))
}

func (a *App) FilterCustomers(w http.ResponseWriter, r *http.Request) {
	handler.FilterUsers(a.DB, w, r, strconv.Itoa(2))
}

func (a *App) GetUser(w http.ResponseWriter, r *http.Request) {
	handler.GetUser(a.DB, w, r)
}

func (a *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUser(a.DB, w, r)
}

func (a *App) CreateUser(w http.ResponseWriter, r *http.Request) {
	handler.CreateUser(a.DB, w, r)
}

// Issue Functions
func (a *App) CreateIssue(w http.ResponseWriter, r *http.Request) {
	handler.CreateIssue(a.DB, w, r)
}

func (a *App) FilterIssues(w http.ResponseWriter, r *http.Request) {
	handler.FilterIssues(a.DB, w, r)
}

func (a *App) GetIssue(w http.ResponseWriter, r *http.Request) {
	handler.GetIssue(a.DB, w, r)
}

func (a *App) UpdateIssue(w http.ResponseWriter, r *http.Request) {
	handler.UpdateIssue(a.DB, w, r)
}

func (a *App) ResolveIssue(w http.ResponseWriter, r *http.Request) {
	handler.ResolveIssue(a.DB, w, r)
}

// Resolution Routes
func (a *App) CreateResolution(w http.ResponseWriter, r *http.Request) {
	handler.CreateResolution(a.DB, w, r)
}

func (a *App) UpdateResolution(w http.ResponseWriter, r *http.Request) {
	handler.UpdateResolution(a.DB, w, r)
}

func (a *App) GetIssueResolutions(w http.ResponseWriter, r *http.Request) {
	handler.GetIssueResolutions(a.DB, w, r)
}

func (a *App) GetDashboardReport(w http.ResponseWriter, r *http.Request) {
	handler.GetDashboardReport(a.DB, w, r)
}

// Query Issues
func (a *App) CreateQueryIssues(w http.ResponseWriter, r *http.Request) {
	handler.CreateQueryIssues(a.DB, w, r)
}

func (a *App) GetQueryIssues(w http.ResponseWriter, r *http.Request) {
	handler.GetQueryIssues(a.DB, w, r)
}

func (a *App) GetCustomersReport(w http.ResponseWriter, r *http.Request) {
	handler.GetCustomersReport(a.DB, w, r)
}

// Channels
func (a *App) CreateChannel(w http.ResponseWriter, r *http.Request) {
	handler.CreateChannel(a.DB, w, r)
}

func (a *App) GetChannels(w http.ResponseWriter, r *http.Request) {
	handler.GetChannels(a.DB, w, r)
}

// Run the app on it's router
func (a *App) Run(host string) {

	allowedHeaders := handlers.AllowedHeaders([]string{"Origin", "X-Requested-With", "Content-Type", "Authorization", "Credentials"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*", "http://localhost:3000", "http://127.0.0.1:3000"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	log.Println("Application started on port >>> ", host)
	log.Fatal(http.ListenAndServe(host, handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(a.Router)))
}

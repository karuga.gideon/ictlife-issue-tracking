package app

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func TestCorsUnexpectedBehaviour(t *testing.T) {

	router := mux.NewRouter()
	router.Path("/ping").Methods("GET", "POST", "OPTIONS").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method == "OPTIONS" {
			w.WriteHeader(200)
			return
		}

		w.WriteHeader(200)
	})

	cors := handlers.CORS(
		handlers.AllowedHeaders([]string{"content-type"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		// handlers.AllowCredentials(),
	)

	router.Use(cors)

	ts := httptest.NewServer(router)
	client := &http.Client{}

	req, _ := http.NewRequest("OPTIONS", ts.URL+"/ping", nil)
	req.Header.Set("Origin", "localhost:3000")
	req.Header.Set("Access-Control-Request-Method", "GET")
	resp, _ := client.Do(req)

	if resp.StatusCode != 200 {
		t.Fatalf("Expected 200 but got %d", resp.StatusCode)
	}
}

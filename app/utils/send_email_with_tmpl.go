package utils

import (
	"bytes"
	"html/template"
	"log"
	"net/smtp"
)

var auth smtp.Auth

// SendEmailTmpl - SendEmail
func SendEmailTmpl() {
	auth = smtp.PlainAuth("", "noreply@getsave.io", "GetSAVE!@!2018", "smtp.getsave.io")
	templateData := struct {
		Name string
		URL  string
	}{
		Name: "Save",
		URL:  "http://getsave.io",
	}
	r := NewRequest([]string{"karuga.gideon@gmail.com"}, "Golang Email Test!", "Hello, World!")
	// err := r.ParseTemplate("email_template.html", templateData)
	// CheckError(err, "Sending Email...")
	if err := r.ParseTemplate("email_template.html", templateData); err == nil {
		ok, _ := r.SendEmail()
		log.Println(ok)
	} else {
		log.Println("Encountered error while sending email...", err.Error())
	}
}

//Request struct
type Request struct {
	from    string
	to      []string
	subject string
	body    string
}

// NewRequest -NewRequest
func NewRequest(to []string, subject, body string) *Request {
	return &Request{
		to:      to,
		subject: subject,
		body:    body,
	}
}

// SendEmail - SendEmail
func (r *Request) SendEmail() (bool, error) {
	mime := "MIME-version: 1.0;\nContent-Type: text/plain; charset=\"UTF-8\";\n\n"
	subject := "Subject: " + r.subject + "!\n"
	msg := []byte(subject + mime + "\n" + r.body)
	addr := "smtp.gmail.com:587"

	if err := smtp.SendMail(addr, auth, "karuga.gideon@gmail.com", r.to, msg); err != nil {
		return false, err
	}
	return true, nil
}

// ParseTemplate - ParseTemplate
func (r *Request) ParseTemplate(templateFileName string, data interface{}) error {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return err
	}
	r.body = buf.String()
	return nil
}

package utils

import (
	"encoding/json"
	"fmt"
	"ictlife-issue-tracking/app/model"
	"log"
	"net/http"
	"strings"

	// "time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/jinzhu/gorm"
	"github.com/mitchellh/mapstructure"
)

// User struct
type User struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	OrgID     int    `json:"org_id"`
	ExpiresAt string `json:"expires_at"`
	Issuer    string `json:"issuer"`
}

// JwtToken struct
type JwtToken struct {
	Status string     `json:"status"`
	Token  string     `json:"token"`
	User   model.User `json:"user"`
}

// Exception struct
type Exception struct {
	Message string `json:"message"`
}

var secretKey = "FASDFLSDF89A0SD8FASDF00FSDFLKJ"

// CreateTokenEndpoint - CreateTokenEndpoint
func CreateTokenEndpoint_001(w http.ResponseWriter, req *http.Request, org_id string) {
	var user User
	_ = json.NewDecoder(req.Body).Decode(&user)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		// "password": user.Password,
		"ExpiresAt": 15000,
		"Issuer":    "pgw",
		"org_id":    org_id})

	// secretKey := "FASDFLSDF89A0SD8FASDF00FSDFLKJ"
	// t := time.Now()
	// secretKey = secretKey+"%d%02d%02d%02d", t.Year(), t.Month(), t.Day(), t.Hour()

	tokenString, error := token.SignedString([]byte(secretKey))
	if error != nil {
		log.Println(error)
	}
	json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
}

// CreateTokenEndpointCustom - CreateTokenEndpointCustom
func CreateTokenEndpointCustom(w http.ResponseWriter, user model.User) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"Email":     user.Email,
		"ExpiresAt": 15000,
		"Issuer":    "ictlife-issue-tracking"})
	tokenString, error := token.SignedString([]byte(secretKey))
	if error != nil {
		log.Println(error)
	}
	randomPwd, _ := EncryptString(GenerateRandomString(10), GenerateRandomString(10))
	user.Password = randomPwd
	json.NewEncoder(w).Encode(JwtToken{Status: "00", Token: tokenString, User: user})
}

// CreateTokenEndpointCustomINITIAL - CreateTokenEndpointCustom
func CreateTokenEndpointCustomINITIAL(db *gorm.DB, w http.ResponseWriter, user *model.User, saveV2Token string) string {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username":  user.Email,
		"password":  user.Password,
		"ExpiresAt": 15000,
		"Issuer":    "nbc",
		"id":        user.ID})

	tokenString, error := token.SignedString([]byte(secretKey))
	if error != nil {
		log.Println(error)
	}

	return tokenString
}

// ProtectedEndpoint - ProtectedEndpoint
func ProtectedEndpoint(w http.ResponseWriter, req *http.Request) {
	params := req.URL.Query()
	token, _ := jwt.Parse(params["token"][0], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var user User
		mapstructure.Decode(claims, &user)
		json.NewEncoder(w).Encode(user)
	} else {
		// json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
		w.WriteHeader(http.StatusUnauthorized)
	}
}

func ValidateMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authorizationHeader := req.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}

					// t := time.Now()
					// secretKey = secretKey+"%d%02d%02d%02d", t.Year(), t.Month(), t.Day(), t.Hour()

					return []byte(secretKey), nil
				})
				if error != nil {
					json.NewEncoder(w).Encode(Exception{Message: error.Error()})
					return
				}

				log.Println("Validating Token...")

				if token.Valid {
					context.Set(req, "decoded", token.Claims)
					next(w, req)
				} else {
					// json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
					w.WriteHeader(http.StatusUnauthorized)
				}
			}
		} else {
			// json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
			w.WriteHeader(http.StatusUnauthorized)
		}
	})
}

func TestEndpoint(w http.ResponseWriter, req *http.Request) {
	decoded := context.Get(req, "decoded")
	var user User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	json.NewEncoder(w).Encode(user)
}

// Get Authenticated User Org id to save in transactions
func GetUserOrgId(req *http.Request) User {
	decoded := context.Get(req, "decoded")
	var user User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	return user
}

// https://www.thepolyglotdeveloper.com/2017/03/authenticate-a-golang-api-with-json-web-tokens/

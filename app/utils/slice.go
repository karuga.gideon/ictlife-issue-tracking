package utils

func Int64SliceContains(list []int64, num int64) bool {
	for _, numInList := range list {
		if num == numInList {
			return true
		}
	}

	return false
}

func SliceContains(list []string, word string) bool {
	for _, wordInList := range list {
		if word == wordInList {
			return true
		}
	}

	return false
}

func SliceRemove(list []string, word string) []string {

	newList := make([]string, 0, len(list))

	for _, wordInList := range list {
		if word != wordInList {
			newList = append(newList, wordInList)
		}
	}

	return newList
}

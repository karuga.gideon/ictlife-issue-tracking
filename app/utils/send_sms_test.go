package utils

import (
	"ictlife-issue-tracking/app/model"
	"log"
	"testing"
)

func TestSendSms(t *testing.T) {

	sms := model.SysMsg{}
	sms.Recipient = "+254725068386"
	sms.Message = "Hi, testing from ICT Life issue tracking."

	sendSms := NewSendSms()
	smsResponse, err := sendSms.SendSMS(sms)

	if err != nil {
		t.Errorf("Failed to send SMS : " + err.Error())
	}

	log.Println("Send SMS Status >>> ", smsResponse.Status)
}

package utils

import (
	"ictlife-issue-tracking/app/model"
	"net/http"
)

// PaginationFilterFromContext - Get Pagination details from query URL
func PaginationFilterFromContext(r *http.Request) model.ReportsFilter {

	reportsFilter := model.ReportsFilter{}
	keys := r.URL.Query()

	if keys.Get("start_date") != "" {
		reportsFilter.StartDate = keys.Get("start_date")
	}

	if keys.Get("end_date") != "" {
		reportsFilter.EndDate = keys.Get("end_date")
	}

	if keys.Get("status") != "" {
		reportsFilter.Status = keys.Get("status")
	}

	if keys.Get("status_id") != "" {
		reportsFilter.StatusID = ConvertStringToInt(keys.Get("status_id"))
	}

	if keys.Get("query") != "" {
		reportsFilter.Query = keys.Get("query")
	}

	if keys.Get("page") != "" {
		reportsFilter.Page = ConvertStringToInt(keys.Get("page"))
	}

	if keys.Get("per") != "" {
		reportsFilter.Per = ConvertStringToInt(keys.Get("per"))
	}

	return reportsFilter

}

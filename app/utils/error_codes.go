package utils

import (
	"fmt"
	"net/http"
)

type ErrorCode string

const (
	ErrorCodeIncorrectPassword           ErrorCode = "invalid_old_password"
	ErrorCodeInvalidAPIKey               ErrorCode = "invalid_api_key"
	ErrorCodeInvalidArgument             ErrorCode = "invalid_argument"
	ErrorCodeInvalidCredentials          ErrorCode = "invalid_credentials"
	ErrorCodeInvalidForm                 ErrorCode = "invalid_form"
	ErrorCodeInvalidPassword             ErrorCode = "invalid_password"
	ErrorCodeInvalidPasswordConfirmation ErrorCode = "invalid_password_confirmation"
	ErrorCodeInvalidPasswordLength       ErrorCode = "invalid_password_length"
	ErrorCodeInvalidToken                ErrorCode = "invalid_token"
	ErrorCodeInvalidUsername             ErrorCode = "invalid_username"
	ErrorCodeInvalidUsernameLength       ErrorCode = "invalid_username_length"
	ErrorCodeInvalidUserStatus           ErrorCode = "invalid_user_status"
	ErrorCodePasswordResetTokenExists    ErrorCode = "password_reset_token_exists"
	ErrorCodeRequestFailed               ErrorCode = "request_failed"
	ErrorCodeRoleActive                  ErrorCode = "role_active"
	ErrorCodeRoleForbidden               ErrorCode = "role_forbidden"
	ErrorCodeSessionExpired              ErrorCode = "session_expired"
	ErrorCodeSQLFailed                   ErrorCode = "sql_failed"
	ErrorCodeUnauthorized                ErrorCode = "unauthorized"
	ErrorCodeUserExistsEmail             ErrorCode = "user_exists_email"
	ErrorCodeUserExistsPhone             ErrorCode = "user_exists_phone"
	ErrorCodeUserExistsUsername          ErrorCode = "user_exits_username"
	ErrorCodeVerificationCodeExpired     ErrorCode = "expired_verification_code"
	ErrorCodeVerificationCodeInvalid     ErrorCode = "invalid_verification_code"
)

var (
	defaultErrorMessage = "Failed to perform request. Please try again."

	errorMessagesMap = map[ErrorCode]string{
		ErrorCodeIncorrectPassword:           "You have provided an incorrect password.",
		ErrorCodeInvalidAPIKey:               "You have provided an invalid api key.",
		ErrorCodeInvalidArgument:             "You have provided an invalid argument.",
		ErrorCodeInvalidCredentials:          "You have provided an invalid email and/or password. Please try again.",
		ErrorCodeInvalidForm:                 "You submitted an invalid form.",
		ErrorCodeInvalidPassword:             "Password must contain valid characters.",
		ErrorCodeInvalidPasswordConfirmation: "Password confirmation does not match password.",
		ErrorCodeInvalidPasswordLength:       fmt.Sprintf("Password must contain at least %d characters", minPasswordLength),
		ErrorCodeInvalidToken:                "You have provided an invalid token.",
		ErrorCodeInvalidUsername:             "Username must contain valid characters.",
		ErrorCodeInvalidUsernameLength:       fmt.Sprintf("Username must contain at least %d characters and at most %d characters", minUsernameLength, maxUsernameLength),
		ErrorCodeInvalidUserStatus:           "Your account is an invalid state.",
		ErrorCodePasswordResetTokenExists:    "Password reset token already exists for user.",
		ErrorCodeRoleActive:                  "Your account is already active. Try logging in.",
		ErrorCodeRoleForbidden:               "You are not permitted to perform request",
		ErrorCodeSessionExpired:              "Session has expired. Try logging in again",
		ErrorCodeUnauthorized:                "You are not authorized to perform request.",
		ErrorCodeUserExistsEmail:             "A user with that email already exists.",
		ErrorCodeUserExistsPhone:             "A user with that phone number already exists.",
		ErrorCodeUserExistsUsername:          "A user with that username already exists.",
		ErrorCodeVerificationCodeExpired:     "Your verification code has expired. Try requesting another token.",
		ErrorCodeVerificationCodeInvalid:     "You have entered an invalid verification code. Try requesting another token.",
	}

	statusCodesMap = map[ErrorCode]int{
		ErrorCodeInvalidUserStatus:  http.StatusNotAcceptable,
		ErrorCodeRoleForbidden:      http.StatusForbidden,
		ErrorCodeSessionExpired:     http.StatusUnauthorized,
		ErrorCodeSQLFailed:          http.StatusInternalServerError,
		ErrorCodeUnauthorized:       http.StatusUnauthorized,
		ErrorCodeUserExistsEmail:    http.StatusConflict,
		ErrorCodeUserExistsPhone:    http.StatusConflict,
		ErrorCodeUserExistsUsername: http.StatusConflict,
	}
)

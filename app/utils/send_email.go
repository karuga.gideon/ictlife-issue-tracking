package utils

import (
	"encoding/base64"
	"ictlife-issue-tracking/app/model"
	"log"
	"net/mail"
	"net/smtp"
	"strings"
)

const (
	logo   = "http://web.getsave.io/assets/img/save_logo(colored).png"
	link   = "https://web.getsave.io/0/{random}"
	footer = "For any inquiry e-mail us on info@getsave.io or call us on +250 785-489-992<p>Thanks for using Save, enjoy. <br>Save Team"
)

// SendEmailViaLinodeServer - SendEmailViaLinodeServer
func SendEmailViaLinodeServer(email model.SysMsg) model.SysMsg {

	log.Println("Sending email >>> ", email.Recipient, email.Subject, email.Message)

	addr := "127.0.0.1:25"
	from := (&mail.Address{"ICTLife Issue Tracking", "tickets@ictlife.com"}).String()
	subject := email.Subject
	body := email.Message

	// []string{(&mail.Address{"to name", "to@example.com"}).String()}
	to := []string{(&mail.Address{"ICTLife Issue Tracking : " + email.Recipient, email.Recipient}).String()}
	err := SendMail(addr, from, subject, body, to)

	if err != nil {
		log.Println("Error sending email.", err.Error())
		email.Status = 3
	} else {
		log.Println("Email sent!")
		email.Status = 2
	}

	return email
}

// SendMail - Via default Server email settings
func SendMail(addr, from, subject, body string, to []string) error {
	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")

	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	if err = c.Mail(r.Replace(from)); err != nil {
		return err
	}
	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	msg := "To: " + strings.Join(to, ",") + "\r\n" +
		"From: " + from + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"Content-Type: text/html; charset=\"UTF-8\"\r\n" +
		"Content-Transfer-Encoding: base64\r\n" +
		"\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	_, err = w.Write([]byte(msg))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	log.Println("Email sent.")
	return c.Quit()
}

package utils

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type Error struct {
	err            error // original error
	errorCode      ErrorCode
	errorMessage   string // Contains error message for error that can be displayed to user
	httpStatusCode int
	logMessages    []string // Contains the "stack" of error messages
	notify         bool     // if true, changes log level to error instead of warn
	userMessages   []string // Contains messages for error that can be displayed to user
}

// Helper constructors

// NewError is the generic constructor which tries to typecast the err input to an Error struct pointer.
// If successful, it adds the log message according to the format specifier.
// Otherwise, it creates a new Error struct using err as the original error.
func NewError(err error, format string, a ...interface{}) *Error {

	appError, ok := err.(*Error)
	if !ok {
		return NewErrorWithCode(
			err,
			ErrorCodeRequestFailed,
			format,
			a...,
		)
	}

	appError.addLogMessage(err, false, format, a...)

	return appError
}

func NewErrorWithCode(err error, errorCode ErrorCode, format string, a ...interface{}) *Error {

	return NewErrorWithErrorCodeAndMessage(
		err,
		errorCode,
		"",
		format,
		a...,
	)
}

func NewErrorWithErrorCodeAndMessage(
	err error,
	errorCode ErrorCode,
	message string,
	format string,
	a ...interface{},
) *Error {

	if errorCode == "" {
		errorCode = ErrorCodeRequestFailed
	}

	appError, ok := err.(*Error)
	if !ok {
		appError = &Error{
			err: err,
		}

		appError.SetErrorCode(errorCode)
	}

	if message != "" {
		appError.errorMessage = message
	}

	appError.addLogMessage(err, true, format, a...)

	return appError
}

// NewDatabaseError creates the Error pointer for any error caused by data repository methods (Find, Save, etc.)
// If the original error is not sql.ErrNoRows, the log messages will trigger an alert to the dev team.
func NewDatabaseError(err error, format string, a ...interface{}) *Error {

	appError := NewError(err, format, a...)

	if err != sql.ErrNoRows {
		appError.SetErrorCode(ErrorCodeSQLFailed)
		appError.httpStatusCode = http.StatusInternalServerError
		appError.Notify()
	} else {
		appError.httpStatusCode = http.StatusNotFound
	}

	return appError
}

func IsErrNoRows(err error) bool {

	appError, ok := err.(*Error)

	if !ok {
		return err == sql.ErrNoRows
	} else {
		return appError.Err() == sql.ErrNoRows
	}
}

func (e *Error) AddErrorMessage(format string, a ...interface{}) *Error {
	e.errorMessage = fmt.Sprintf(format, a...)
	return e
}

// Error interface
func (e *Error) Error() string {

	prefix := "[WARN]"

	if e.notify {
		prefix = "[ERROR]"
	}

	return fmt.Sprintf("%s %s", prefix, strings.Join(e.logMessages, "; "))
}

func (e *Error) Err() error {
	return e.err
}

// Utility Functions

func (e *Error) HttpStatus() int {

	if e.httpStatusCode != 0 {
		return e.httpStatusCode
	}

	mapHttpStatus, ok := statusCodesMap[e.errorCode]
	if !ok || mapHttpStatus == 0 {
		return http.StatusBadRequest
	}

	return mapHttpStatus
}

func (e *Error) JsonResponse() map[string]string {

	if e.errorMessage == "" {
		e.errorMessage = "Failed to perform request. Please try again."
	}

	return map[string]string{
		"error_message": e.errorMessage,
	}
}

func (e *Error) LogErrorMessages() {
	log.Println(e.Error())
}

func (e *Error) Notify() *Error {
	e.notify = true
	return e
}

func (e *Error) SetErrorCode(errorCode ErrorCode) {
	e.errorCode = errorCode
	mapErrorMessage, ok := errorMessagesMap[errorCode]
	if !ok || mapErrorMessage == "" {
		e.errorMessage = defaultErrorMessage
	} else {
		e.errorMessage = mapErrorMessage
	}
}

// Private functions
func (e *Error) addLogMessage(err error, includeErr bool, format string, a ...interface{}) {

	suffix := ""
	if includeErr {
		suffix = fmt.Sprintf(" because err=[%v]", err)
	}

	e.logMessages = append([]string{fmt.Sprintf("%s%s", fmt.Sprintf(format, a...), suffix)}, e.logMessages...)
}

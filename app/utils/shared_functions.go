package utils

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	cryptoRand "crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"ictlife-issue-tracking/app/model"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/mitchellh/mapstructure"
	"github.com/vjeantet/jodaTime"
)

// var PassPhrase = "USUCitKwDMONwYuZMRyEwOMuzXNSrGpuzvdvUrgW"

// ConvertStringToInt - ConvertStringToInt
func ConvertStringToInt(str string) int {
	j, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return j
}

func ConvertStringToInt64(str string) int64 {
	n, err := strconv.ParseInt(str, 10, 64)
	if err == nil {
		return n
	}
	return 0
}

func ConvertStringToFloat64(str string) float64 {
	var floatvalue float64
	floatvalue, _ = strconv.ParseFloat(str, 64)
	return floatvalue
}

func ConvertFormatTwoStringDateToDate(dateString string) time.Time {
	log.Println("Converting string to date >>>> ", dateString)
	// 25/07/2019
	dateString = FormatDateTypeOneString(dateString)
	layout := "2006-01-02"
	str := dateString
	t, err := time.Parse(layout, str)
	if err != nil {
		log.Println(err)
		t = time.Now()
	}
	log.Println(t)
	return t
}

// GetErrorCode -- Gets the error code  / message from MTN - Error Response
func GetErrorCode(errorString string) string {

	var errorCode = errorString
	errorStart := strings.LastIndex(errorCode, "errorcode")
	// log.Println("errorStart : %v\n", errorStart)
	errorCode = errorCode[errorStart:len(errorCode)]
	errorEnd := strings.Index(errorCode, ">")
	// log.Println("errorEnd : %v\n", errorEnd)

	errorCode = errorString[errorStart:(errorStart + errorEnd)]
	errorCode = strings.Replace(errorCode, "errorcode=", "", 1)
	errorCode = strings.Replace(errorCode, "/", "", 1)
	errorCode = strings.Replace(errorCode, "\"", "", 2)
	// log.Println("errorCode is: %v\n", errorCode)
	return errorCode
}

// GenerateRandomString - Generates a rand string of n - length
func GenerateRandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// DecryptString - ENCRYPTION AND DECRYPTION **/
// Takes two strings, cryptoText and keyString.
// cryptoText is the text to be decrypted and the keyString is the key to use for the decryption.
// The function will output the resulting plain text string with an error variable.
func DecryptString(cryptoText string, keyString string) (plainTextString string, err error) {

	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)

	// Encode the cryptoText to base 64.
	cipherText, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher([]byte(newKeyString))

	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		panic("cipherText too short")
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}

// EncryptString - Takes two string, plainText and keyString.
// plainText is the text that needs to be encrypted by keyString.
// The function will output the resulting crypto text and an error variable.
func EncryptString(plainText string, keyString string) (cipherTextString string, err error) {

	// Format the keyString so that it's 32 bytes.
	newKeyString, err := hashTo32Bytes(keyString)

	if err != nil {
		return "", err
	}

	key := []byte(newKeyString)
	value := []byte(plainText)

	block, err := aes.NewCipher(key)

	if err != nil {
		panic(err)
	}

	cipherText := make([]byte, aes.BlockSize+len(value))

	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(cryptoRand.Reader, iv); err != nil {
		return
	}

	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], value)

	return base64.URLEncoding.EncodeToString(cipherText), nil
}

// As we cannot use a variable length key, we must cut the users key
// up to or down to 32 bytes. To do this the function takes a hash
// of the key and cuts it down to 32 bytes.
func hashTo32Bytes(input string) (output string, err error) {

	if len(input) == 0 {
		return "", errors.New("No input supplied")
	}

	hasher := sha256.New()
	hasher.Write([]byte(input))

	stringToSHA256 := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	// Cut the length down to 32 bytes and return.
	return stringToSHA256[:32], nil
}

// DelaySeconds - delay for a number of seconds
func DelaySeconds(n time.Duration) {
	time.Sleep(n * time.Second)
}

// CheckError - check for err globally
func CheckError(err error, description string) {
	// raven.SetDSN("https://4721f61661b945cd97dc2eeea0230658:6aecf01aeaa6470b91b7b9e4d9ffd9da@sentry.io/243952")
	if err != nil {
		// panic(er) // CaptureErrorAndWait // CaptureError
		log.Println("Err ecountered on >>> ", "ESD Ws :::: "+description)
		log.Println("Encountered an Error >>> ", err.Error())
		// result := raven.CaptureError(err, nil)
		// log.Println("Go sentry >>> ", result)
		// result = raven.CaptureErrorAndWait(err, nil)
		// log.Println("Go sentry [2] >>> ", result)
	}
}

// GetAccountProvider - This case - for MTN and TIGO
func GetAccountProvider(account string) string {

	networkProvider := strings.HasPrefix(account, "25072") // true - tigo
	log.Println("Network Provider [2] >>> ", networkProvider)

	if networkProvider == true {
		return "TIGO"
	} else {
		// 25078
		return "MTN"
	}
}

// Base64DecodeString - Base64DecodeString
func Base64DecodeString(str string) string {
	data, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		log.Println("error:", err)
		return ("")
	}
	// fmt.Printf("%q\n", data)
	strData := fmt.Sprintf("%s", data)
	return strData
}

// ConvertIntToString - ConvertIntToString
func ConvertIntToString(num int) string {
	t := strconv.Itoa(num)
	return t
}

// StartDate - StartDate
func StartDate(num int) string {
	// For db date format data retrieval
	t := strconv.Itoa(num)
	if num < 10 {
		t = "0" + t
	}
	t = t + "-01"
	return t
}

// EndDate - EndDate
func EndDate(num int) string {
	// For db date format data retrieval
	t := strconv.Itoa(num)
	if num < 10 {
		t = "0" + t
	}

	switch num {
	case 1:
		t = t + "-31"
	case 2:
		t = t + "-28"
	case 3:
		t = t + "-31"
	case 4:
		t = t + "-30"
	case 5:
		t = t + "-31"
	case 6:
		t = t + "-30"
	case 7:
		t = t + "-31"
	case 8:
		t = t + "-31"
	case 9:
		t = t + "-30"
	case 10:
		t = t + "-31"
	case 11:
		t = t + "-30"
	case 12:
		t = t + "-31"
	}

	return t
}

// GetJwtUserID - Get Authenticated User id to save in transactions
func GetJwtUserID(req *http.Request) int {
	decoded := context.Get(req, "decoded")
	// log.Println("Decoded decoded >>> ", decoded)
	var user model.User
	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
	return user.ID
}

// Round - Round
func Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

// ToFixed - ToFixed
func ToFixed(num float64, precision int) float64 {
	// log.Println(toFixed(1.2345678, 0))  // 1
	// log.Println(toFixed(1.2345678, 1))  // 1.2
	// log.Println(toFixed(1.2345678, 2))  // 1.23
	// log.Println(toFixed(1.2345678, 3))  // 1.235 (rounded up)
	output := math.Pow(10, float64(precision))
	return float64(Round(num*output)) / output
}

// URLEncodeString encodes a string like Javascript's encodeURIComponent()
func URLEncodeString(str string) string {
	u, err := url.Parse(str)
	if err != nil {
		log.Println("Error encoding URL String >>> ", str)
		return ""
	}
	return u.String()
}

// FloatToString - FloatToString
func FloatToString(inputNum float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(inputNum, 'f', 2, 64)
}

// FormatNumber - FormatNumber
func FormatNumber(n int64) string {
	in := strconv.FormatInt(n, 10)
	out := make([]byte, len(in)+(len(in)-2+int(in[0]/'0'))/3)
	if in[0] == '-' {
		in, out[0] = in[1:], '-'
	}

	for i, j, k := len(in)-1, len(out)-1, 0; ; i, j = i-1, j-1 {
		out[j] = in[i]
		if i == 0 {
			return string(out)
		}
		if k++; k == 3 {
			j, k = j-1, 0
			out[j] = ','
		}
	}
}

// FormatFloat - Float to string for easier message reading
func FormatFloat(n float64) string {
	return toString(n)
}

func commas(s string) string {
	if len(s) <= 3 {
		return s
	} else {
		return commas(s[0:len(s)-3]) + "," + s[len(s)-3:]
	}
}

func toString(f float64) string {
	parts := strings.Split(fmt.Sprintf("%.2f", f), ".")
	if parts[0][0] == '-' {
		return "-" + commas(parts[0][1:]) + "." + parts[1]
	}
	return commas(parts[0]) + "." + parts[1]
}

// StringToFloat func
func StringToFloat(value string) float64 {
	if s, err := strconv.ParseFloat(value, 64); err == nil {
		// fmt.Printf("%T, %v\n", s, s)
		return s
	} else {
		return 0
	}
}

// CopyFile - Copy the src file to dst. Any existing file will be overwritten and will not copy file attributes.
func CopyFile(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}

// FileExists - Check whether a file exists or not
func FileExists(name string) bool {
	_, err := os.Stat(name)
	return !os.IsNotExist(err)
}

func ConvertPDFtoTxt(srcFile, destFile string) {
	// exec.Command("pdftotext.exe", "20140523151621.pdf", "text_001.txt").Run()
	exec.Command("pdftotext.exe", srcFile, destFile).Run()
	lineCount := TextFileLineCount(destFile)
	log.Println("lineCount  >>>>  ", lineCount)
}

func DeleteFile(path string) {
	// path := "/path/to/file/to/delete"
	log.Println("Deleting File >>> ", path)
	err := os.Remove(path)

	if err != nil {
		log.Println(err)
		return
	}
}

// IndexOf - Find the index of a string in a string array
func IndexOf(word string, data []string) int {
	for k, v := range data {
		if word == v {
			return k
		}
	}
	return -1
}

// WriteToTxtFile - WriteToTxtFile
func WriteToTxtFile(destFile, content string) {
	// file, err := os.OpenFile("output.txt", os.O_WRONLY|os.O_CREATE, 0666)
	log.Println("Creating txt file >>> ", destFile)
	file, err := os.OpenFile(destFile, os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		log.Println("File does not exists or cannot be created")
		os.Exit(1)
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	fmt.Fprintf(w, "%v\n", content)

	w.Flush()
}

// PrintDocument - PrintDocument - documentURL, printerName
func PrintDocument(documentURL, printerName string) {
	// Printer_Utils.exe document_url printer
	log.Println("Printing document >>> ", documentURL, ", to printer >>> ", printerName)
	exec.Command("Printer_Utils.exe", documentURL, printerName).Run()
}

// FormatDateTypeOneDate - 03/11/06 to time
func FormatDateTypeOneDate(dateString string) time.Time {
	// str := "03/11/06"
	dateArr := strings.Split(dateString, "/")

	day := dateArr[0]
	month := dateArr[1]
	year := dateArr[2]

	log.Println("day : ", day, ", month: ", month, ", year :", year)

	if len(year) < 4 {
		year = "20" + year
	}
	log.Println("New year >>> ", year)

	layout := "2006-01-02"
	// str := "2014-11-12T11:45:26.371Z"
	newDateStr := year + "-" + month + "-" + day
	t, err := time.Parse(layout, newDateStr)

	if err != nil {
		log.Println(err)
	}
	log.Println(t)
	return t
}

// FormatDateTypeOneString - 03/11/06 to string
func FormatDateTypeOneString(dateString string) string {
	// str := "03/11/06"
	dateArr := strings.Split(dateString, "/")
	newDateStr := GetTodaysInvoiceDate()
	if len(dateArr) >= 2 {
		day := dateArr[0]
		month := dateArr[1]
		year := dateArr[2]

		log.Println("day : ", day, ", month: ", month, ", year :", year)

		if len(year) < 4 {
			year = "20" + year
		}
		log.Println("New year >>> ", year)

		// layout := "2006-01-02"
		// str := "2014-11-12T11:45:26.371Z"
		newDateStr = year + "-" + month + "-" + day
		log.Println(newDateStr)
	}
	return newDateStr
}

// GetTodaysInvoiceDate - save the date to db for an invoice when not able to process the invoices date
func GetTodaysInvoiceDate() string {

	timeNow := time.Now()

	// Day
	day := timeNow.Day()
	dayString := ConvertIntToString(day)
	if len(dayString) < 2 {
		dayString = "0" + dayString
	}

	// Month
	month := timeNow.Month()
	monthInt := int(month)
	monthString := ConvertIntToString(monthInt)
	if len(monthString) < 2 {
		monthString = "0" + monthString
	}

	// Year
	year := timeNow.Year()
	yearString := ConvertIntToString(year)

	// log.Println("day : ", dayString, ", month: ", monthString, ", year :", yearString)
	dateString := yearString + "-" + monthString + "-" + dayString
	// log.Println("dateString  >>> ", dateString)
	return dateString
}

// FormatDateTypeTwoString - // 2. April 2019 to string
func FormatDateTypeTwoString(dateString string) string {
	// str := "// 2. April 2019"
	// 31 MAY 2019
	dateString = strings.Replace(dateString, ".", "", -1)
	dateString = strings.Replace(dateString, ",", "", -1)
	dateArr := strings.Split(dateString, " ")
	newDateStr := ""
	if len(dateArr) >= 2 {
		day := dateArr[0]
		month := dateArr[1]
		year := dateArr[2]

		log.Println("day : ", day, ", month: ", month, ", year :", year)

		if len(year) < 4 {
			year = "20" + year
		}
		log.Println("New year >>> ", year)

		if len(day) < 2 {
			day = "0" + day
		}

		switch month {
		case "JANUARY":
			month = "01"
		case "FEBRUARY":
			month = "02"
		case "MARCH":
			month = "03"
		case "APRIL":
			month = "04"
		case "MAY":
			month = "05"
		case "JUNE":
			month = "06"
		case "JULY":
			month = "07"
		case "AUGUST":
			month = "08"
		case "SEPTEMBER":
			month = "09"
		case "OCTOBER":
			month = "10"
		case "NOVEMBER":
			month = "11"
		case "DECEMBER":
			month = "12"
		}

		// layout := "2006-01-02"
		// str := "2014-11-12T11:45:26.371Z"
		newDateStr = year + "-" + month + "-" + day
		log.Println(newDateStr)
	}
	return newDateStr
}

// FormatDateTypeThreeString - // 2. April 2019 to string
func FormatDateTypeThreeString(dateString string) string {
	// str := "// 2. April 2019"
	dateString = strings.Replace(dateString, ".", "", -1)
	dateString = strings.Replace(dateString, ",", "", -1)
	dateArr := strings.Split(dateString, " ")
	newDateStr := ""
	if len(dateArr) >= 2 {
		day := dateArr[0]
		month := dateArr[1]
		year := dateArr[2]

		log.Println("day : ", day, ", month: ", month, ", year :", year)

		if len(year) < 4 {
			year = "20" + year
		}
		log.Println("New year >>> ", year)

		if len(day) < 2 {
			day = "0" + day
		}

		switch month {
		case "JAN":
			month = "01"
		case "FEB":
			month = "02"
		case "MAR":
			month = "03"
		case "APR":
			month = "04"
		case "MAY":
			month = "05"
		case "JUN":
			month = "06"
		case "JUL":
			month = "07"
		case "AUG":
			month = "08"
		case "SEP":
			month = "09"
		case "OCT":
			month = "10"
		case "NOV":
			month = "11"
		case "DEC":
			month = "12"
		}

		// layout := "2006-01-02"
		// str := "2014-11-12T11:45:26.371Z"
		newDateStr = year + "-" + month + "-" + day
		log.Println(newDateStr)
	}
	return newDateStr
}

// TodaysSignedFolder - Create a folder with todays date
func TodaysSignedFolder() string {
	// yyyy_MM_dd
	date := jodaTime.Format("YYYY_MM_dd", time.Now())
	// log.Println(date)

	dateString := "" + date
	// log.Println("dateString  >>>> ", dateString)
	// dateTime, _ := jodaTime.Parse("dd/MMMM/yyyy:HH:mm:ss", "30/August/2015:21:44:25")
	// log.Println(dateTime.String())

	return dateString
}

// ConvertFormatedStringDateToDate - for invoice saving to db
func ConvertFormatedStringDateToDate(dateString string) time.Time {
	log.Println("Converting string to date >>>> ", dateString)
	layout := "2006-01-02"
	str := dateString
	t, err := time.Parse(layout, str)
	if err != nil {
		log.Println(err)
		t = time.Now()
	}
	log.Println(t)
	return t
}

func TextFileLineCount(txtFile string) int {

	lineCount := 0
	file, err := os.Open(txtFile)
	CheckError(err, "Error reading signed input file >>> "+txtFile)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		lineCount++
	}

	return lineCount

}

func SaveLastTwoHundredLines(startFrom int, srcFile string, destFile string) {

	lineCount := 0
	file, err := os.Open(srcFile)
	CheckError(err, "Error saving last 200 lines for file >>> "+srcFile)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	content := ""

	for scanner.Scan() {
		if startFrom > lineCount {
			invoiceLine := scanner.Text()
			content += invoiceLine + "\n"
		}
		lineCount++
	}

	WriteToTxtFile(destFile, content)

}

func FormatReportDateStringTimestamp(timestampString string) time.Time {
	// Step 1: Convert it to a rune
	a := []rune(timestampString)

	// Step 2: Grab the num of chars you need
	myShortString := string(a[0:10])

	// Convert to time.Time
	i, err := strconv.ParseInt(myShortString, 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0)

	return tm.UTC()
}

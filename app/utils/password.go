package utils

import (
	"errors"
	"ictlife-issue-tracking/app/model"

	"regexp"
	"time"

	"golang.org/x/crypto/bcrypt"
)

var invalidPasswordRegex = regexp.MustCompile("[[:^graph:]]")
var minPasswordLength = 8

// ComparePassword checks if password and password hash match
func ComparePassword(passwordHash string, password string) error {
	passwordHashBytes := []byte(passwordHash)
	passwordBytes := []byte(password)
	return bcrypt.CompareHashAndPassword(passwordHashBytes, passwordBytes)
}

// GeneratePasswordHash generates the password hash from password
func GeneratePasswordHash(password string) (string, error) {
	saltedBytes := []byte(password)
	hashedBytes, err := bcrypt.GenerateFromPassword(saltedBytes, 12)
	if err != nil {
		return "", err
	}

	passwordHash := string(hashedBytes[:])
	return passwordHash, nil
}

// ValidatePassword checks if password has enough characters and contains only valid characters
func ValidatePassword(password string) error {

	if len(password) < minPasswordLength {
		return NewErrorWithCode(
			errors.New("invalid password length"),
			ErrorCodeInvalidPasswordLength,
			"Failed to validate password of length %d",
			len(password),
		)
	}

	loc := invalidPasswordRegex.FindStringIndex(password)
	if loc != nil {
		return NewErrorWithCode(
			errors.New("invalid password"),
			ErrorCodeInvalidPassword,
			"Failed to validate password with invalid characters",
		)
	}

	return nil
}

// ValidatePasswordResetToken checks whether a user has a token, and if they do, whether it's still valid
func ValidatePasswordResetToken(user *model.User) error {

	if user.ResetPasswordTokenExpiresAt.Valid && user.ResetPasswordTokenExpiresAt.Time.After(time.Now()) {
		return NewErrorWithCode(
			errors.New("valid reset password token exists"),
			ErrorCodePasswordResetTokenExists,
			"User has an existing valid password reset token.",
		)
	}

	return nil

}

package utils

import (
	"errors"
	"regexp"
)

var invalidUsernameRegex = regexp.MustCompile("[[:^word:]]")
var minUsernameLength = 3
var maxUsernameLength = 15

// ValidateUsername checks if username has enough characters and contains only valid characters
func ValidateUsername(username string) error {

	if len(username) < minUsernameLength || len(username) > maxUsernameLength {
		return NewErrorWithCode(
			errors.New("invalid username length"),
			ErrorCodeInvalidUsernameLength,
			"Failed to validate username of length %d",
			len(username),
		)
	}

	loc := invalidUsernameRegex.FindStringIndex(username)
	if loc != nil {
		return NewErrorWithCode(
			errors.New("invalid username"),
			ErrorCodeInvalidUsername,
			"Failed to validate username with invalid characters",
		)
	}

	return nil
}

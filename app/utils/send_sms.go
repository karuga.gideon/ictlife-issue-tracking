package utils

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"ictlife-issue-tracking/app/model"
	"ictlife-issue-tracking/config"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Destination struct {
	Number string `json:"number"`
}

type SMSMessage struct {
	Source       string        `json:"source"`
	Message      string        `json:"message"`
	Destination  []Destination `json:"destination"`
	StatusURL    string        `json:"status_url"`
	StatusSecret string        `json:"status_secret"`
}

// SendSMS - Via SMS Leopard
func SendSMS(message model.SysMsg) (model.SysMsg, error) {

	config := config.GetConfig()
	auth := config.SMSCredentials.Username + ":" + config.SMSCredentials.Password
	authenticationString := base64.StdEncoding.EncodeToString([]byte(auth))

	url := "https://api.smsleopard.com/v1/sms/send"

	smsMessage := SMSMessage{}
	smsMessage.Source = "SMSLEOPARD"
	smsMessage.Message = message.Message

	destination := Destination{}
	destination.Number = message.Recipient

	var destinations []Destination
	destinations = append(destinations, destination)

	smsMessage.Destination = destinations
	smsMessage.StatusURL = "https://your-status.url"
	smsMessage.StatusSecret = "supersecret"

	smsPayload, err := json.Marshal(smsMessage)
	if err != nil {
		return message, NewError(
			err,
			"Unable to Marshal SMS Object.",
		)
	}

	// payload := strings.NewReader("{\n\t\"source\":\"" + config.SMSCredentials.SenderID + "\",\n\t\"message\": \"" + message.Message + "\",        \n\t\"destination\": \n\t\t[            \n\t\t\t{\"number\": \"" + message.Recipient + "\"}\n\t\t],        \n\t\"status_url\": \"" + config.SMSCredentials.StatusURL + "\",        \n\t\"status_secret\": \"supersecret\"    \n}")

	smsPayloadString := string(smsPayload)
	log.Println("smsPayloadString  >>> ", smsPayloadString)

	payload := strings.NewReader(smsPayloadString)
	req, err := http.NewRequest("POST", url, payload)

	if err != nil {
		return message, NewError(
			err,
			"Unable to process send_sms request.",
		)
	}

	req.Header.Add("Content-Type", "application/json")
	// req.Header.Add("Authorization", "Basic NEVVczA5Z1hHWkM2U1NmczFWRzU6U2FvSVFlSFIyY0RWcFhxc3BVbGFCSHlaN2x1Tm5tNG5HaVhWSTVOSg==")
	req.Header.Add("Authorization", "Basic "+authenticationString)
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Host", "api.smsleopard.com")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	// req.Header.Add("Content-Length", "236")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("cache-control", "no-cache")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return message, NewError(
			err,
			"Unable to process sms response body.",
		)
	}

	fmt.Println("Sms Response full  >>> ", res)
	fmt.Println("Sms Response Body  >>> ", string(body))
	fmt.Println("Response Status >>>> ", res.StatusCode)

	if res.StatusCode == http.StatusCreated {
		message.Status = 2
	} else {
		message.Status = 3
		return message, NewError(
			err,
			"Failed to send sms message: "+string(body),
		)
	}

	return message, nil

}

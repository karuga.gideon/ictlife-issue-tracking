package model

import (
	"time"

	"github.com/jinzhu/gorm"
	"gopkg.in/guregu/null.v3"
)

// Activity table struct
type Activity struct {
	ID           int       `json:"id"`
	UserID       int       `json:"user_id"`
	ActivityType string    `json:"activity_type"`
	Description  string    `json:"description"`
	SourceIP     string    `json:"source_ip"`
	ResourceID   int       `json:"resource_id"`
	CreatedOn    string    `json:"created_on"`
	DateCreated  time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
}

// User struct
type User struct {
	ID                          int         `json:"id"`
	DateCreated                 time.Time   `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
	DateUpdated                 time.Time   `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" json:"date_updated"`
	Name                        string      `json:"name"`
	Phone                       string      `json:"phone"`
	Email                       string      `json:"email"`
	Password                    string      `json:"password"`
	Type                        int         `json:"type"` // 1 - CX_Member, 2 - Customer
	ResetPasswordTokenExpiresAt null.Time   `db:"reset_password_token_expires_at" json:"reset_password_token_expires_at"`
	ResetPasswordToken          null.String `db:"reset_password_token" json:"reset_password_token"`
}

// Issue struct
type Issue struct {
	ID            int       `json:"id"`
	DateCreated   time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
	DateUpdated   time.Time `gorm:"column:date_updated;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" json:"date_updated"`
	CustomerID    int       `json:"customer_id"`
	CustomerEmail string    `json:"customer_email"`
	ChannelID     int       `json:"channel_id"`
	QueryIssue    string    `json:"query_issue"`
	IssueDetails  string    `json:"issue_details"`
	AssignedTo    int       `json:"assigned_to"`
	StatusID      int       `json:"status_id"` // 0 - Open, 1 - Ongoing, 2 - Resolved, 3 - Follow up required.
	Action        string    `json:"action"`
	CreatedBy     string    `json:"created_by"` // CX_TEAM | CUSTOMER
	UpdatedAt     time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" json:"updated_at"`
}

// Resolution struct
type Resolution struct {
	ID          int       `json:"id"`
	IssueID     int       `json:"issue_id"`
	Resolution  string    `json:"resolution"`
	StatusID    int       `json:"status_id"` // 0 - Open, 1 - Ongoing, 2 - Resolved, 3 - Follow up required.
	DateCreated time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
	CXMemberID  int       `json:"cx_member_id"`
}

// Query Issue Type
type QueryIssue struct {
	ID          int       `json:"id"`
	IssueType   string    `json:"issue_type"`
	CreatedBy   int       `json:"created_by"`
	DateCreated time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
}

// Channel used for communicating the issue, either email, phone call, etc.
type CommChannel struct {
	ID          int       `json:"id"`
	ChannelType string    `json:"channel_type"`
	CreatedBy   int       `json:"created_by"`
	DateCreated time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
}

// SysMsg struct
type SysMsg struct {
	ID               int       `json:"id"`
	DateCreated      time.Time `gorm:"column:date_created;not null;default:CURRENT_TIMESTAMP" json:"date_created"`
	MsgType          string    `json:"msg_type"` // EMAIL or SMS
	Recipient        string    `json:"recipient"`
	Subject          string    `json:"subject"`
	Message          string    `json:"message"`
	NotificationType string    `json:"notification_type"`
	Status           int       `json:"status"` // 0 - New, 1 - Processing, 2 - Sent, 3 - Failed
	SgMemberID       int       `json:"sg_member_id"`
	DateSent         time.Time `json:"date_sent"`
	IssueID          int       `json:"issue_id"`
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&User{}, &Activity{}, &Issue{}, &Resolution{}, &QueryIssue{}, &CommChannel{}, &SysMsg{})
	return db
}

// http://doc.gorm.io/models.html#conventions

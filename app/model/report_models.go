package model

// ReportsFilter - From URL Query Params
type ReportsFilter struct {
	StartDate string `json:"start_date"`
	EndDate   string `json:"end_date"`
	Status    string `json:"status"`
	StatusID  int    `json:"status_id"`
	Query     string `json:"query"`
	Page      int    `json:"page"`
	Per       int    `json:"per"`
}

// DashboardReport - default to return issues reports
type DashboardReport struct {
	IssuesReport        IssuesReport          `json:"issues_report"`
	MonthlyIssuesReport MonthlyIssuesReport   `json:"monthly_issues_report"`
	YearlyIssuesReport  []MonthlyIssuesReport `json:"yearly_issues_report"`
}

// IssuesReport - Count of open, ongoing and resolved issues
type IssuesReport struct {
	TotalOpen             int64 `json:"total_open"`
	TotalOngoing          int64 `json:"total_ongoing"`
	TotalResolved         int64 `json:"total_resolved"`
	TotalFollowUpRequired int64 `json:"total_follow_up_required"`
	GrandTotal            int64 `json:"grand_total"`
}

// MonthlyIssuesReport - Issues by month
type MonthlyIssuesReport struct {
	Month        int          `json:"month"`
	IssuesReport IssuesReport `json:"issues_report"`
}

// CustomersReport - Count all customers as well as by month
type CustomersReport struct {
	AllCustomersCount      int64                    `json:"all_customers_count"`
	MonthlyCustomersReport []MonthlyCustomersReport `json:"monthly_customers_report"`
}

// MonthlyCustomersReport - Total customers registered for that month
type MonthlyCustomersReport struct {
	Month          int   `json:"month"`
	CustomersCount int64 `json:"customers_count"`
}

// Result - db result
type Result struct {
	Total float64 `json:"total"`
}

// JSONResponse : Success response
// swagger:response jsonResponse
type JSONResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// BadRequestResponse 400 - bad request - invalid details / wrong data type in request.
// swagger:response badRequestResponse
type BadRequestResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// ForbiddenResponse 403 - you are forbidden from accessing this resource.
// swagger:response forbidddenResponse
type ForbiddenResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}

// NotFoundResponse 404 - resource not found.
// swagger:response notFoundResponse
type NotFoundResponse struct {
	Status      string `json:"status"`
	Description string `json:"description"`
}
